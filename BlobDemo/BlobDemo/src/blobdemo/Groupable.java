/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author alex
 */
public interface Groupable {
    ArrayList<Groupable> ChildrenList();
    boolean hasChildren();                      //check if its a leaf node
    boolean contains(double x,double y);
    boolean inRectangle(double x1,double y1,double x2,double y2);
    void draw(GraphicsContext gc,boolean selected);
    void move(double dx,double dy);
    
    
    double getLeft();   // bounding box of the group 
    double getRight();  
    double getTop();
    double getBottom();
    void setZ(int newZ); // Z-order for the group int getZ();
    int getZ();      //level 1 should on top,level 3000 is on bottom
    Groupable clone();
          
}
