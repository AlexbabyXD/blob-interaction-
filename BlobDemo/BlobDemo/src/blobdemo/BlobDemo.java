/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;


import java.util.Stack;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
/**
 *
 * @author gutwin
 */
public class BlobDemo extends Application {

    BlobView view;
    BlobViewController controller;
    BlobModel model;
    InteractionModel iModel;
    BlobClipboard clipBoard;
    Label clipboardText;
    
    ObservableList<String> undoObserList = FXCollections.observableArrayList();
    ObservableList<String> redoObserList = FXCollections.observableArrayList();


    @Override
    public void start(Stage primaryStage) {
        view = new BlobView(700, 600);
        controller = new BlobViewController();
        model = new BlobModel();
        iModel = new InteractionModel();
        clipBoard = new BlobClipboard();


        controller.setDemo(this);
        controller.setClipBoard(clipBoard);
        controller.setModel(model);
        controller.setInteractionModel(iModel);
        
        view.setModel(model);
        view.setInteractionModel(iModel);
        model.addSubscriber(view);
        iModel.addSubscriber(view);
        Button undoButton = new Button("Undo!");
        Button redoButton = new Button("Redo!");
        clipboardText = new Label("Clipboard : Empty");

        


        
        view.setOnMousePressed(controller::handleMousePressed);
        view.setOnMouseDragged(controller::handleMouseDragged);
        view.setOnMouseReleased(controller::handleMouseReleased);
//        view.setOnMouseClicked(controller::handleMouseClicked);
        view.setOnKeyPressed(controller::handleKeyPressed);
        view.setOnKeyReleased(controller::handleKeyReleased);

        VBox vbox1 = new VBox();
        ListView<String> undoList = new ListView<String>();
        undoList.setItems(undoObserList);
        
        vbox1.getChildren().addAll(undoList,undoButton);
        
        VBox vbox2 = new VBox();
        ListView<String> redoList = new ListView<String>();
        redoList.setItems(redoObserList);
        vbox2.getChildren().addAll(redoList,redoButton);
        
        VBox vbox3 = new VBox();
        vbox3.getChildren().addAll(view,clipboardText);

        
        undoButton.setOnAction(new EventHandler<ActionEvent>(){          
            @Override
            public void handle(ActionEvent event) {
                if(!controller.undoStack.empty()){
                    BlobCommand newCommand;
                    newCommand = controller.undoStack.pop();
                    newCommand.undo();
                    controller.redoStack.push(newCommand);
                    updateUndoStack(controller.undoStack);
                    updateRedoStack(controller.redoStack);
                    
                }
               
             model.notifySubscribers();
             view.requestFocus();

            }    
        });
        
        redoButton.setOnAction(new EventHandler<ActionEvent>(){          
            @Override
            public void handle(ActionEvent event) {
                if(!controller.redoStack.empty()){
                    BlobCommand newCMD;
                   
                    newCMD=controller.redoStack.pop();
                    newCMD.redo();
                    controller.undoStack.push(newCMD);
                    updateRedoStack(controller.redoStack);
                    updateUndoStack(controller.undoStack);

                }
               
             model.notifySubscribers();
             view.requestFocus();


            }    
        });
        
        HBox hbox = new HBox();
        hbox.getChildren().addAll(vbox1,vbox3,vbox2);
        Scene scene = new Scene(hbox);
        
        primaryStage.setScene(scene);
        primaryStage.show();

        view.draw();
        view.requestFocus();        //keyfocus 
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    public void updateUndoStack(Stack x){
        undoObserList.clear();
        for(int i=x.size();i>0;i--){
            undoObserList.add(x.get(i-1).toString());
        }

        }
    public void updateRedoStack(Stack p){
        redoObserList.clear();
        for(int i=p.size();i>0;i--){
            redoObserList.add(p.get(i-1).toString());
        }    
    }
    
    
    

 }


