/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;

/**
 *
 * @author alex
 */
public class MoveCommand implements BlobCommand{

    ArrayList<Groupable> list = new ArrayList<Groupable>();
    String text;
    BlobModel model;
    InteractionModel iModel;
    double prevX;
    double prevY;
    double newX;
    double newY;
    double movePositionX;
    double movePositionY;
    
    public MoveCommand(ArrayList<Groupable> k,double dx,double dy,BlobModel x,InteractionModel imodel){
        
        for(int i=0;i<k.size();i++){
            list.add(k.get(i));
        }
        text = "Move Blob : "+dx+", " + dy;  
        newX=dx;
        newY=dy;        
        model=x;
        iModel=imodel;

    }
    @Override
    public void redo() {
        
        iModel.selectionSet.clear();
        for(int i=0;i<list.size();i++){
            iModel.selectionSet.add(list.get(i));
        }
        model.moveBlobs(list, -movePositionX, -movePositionY);
    }
    
    public void setStartPoint(double x,double y){
        prevX=x;
        prevY=y;
    }
    
    @Override
    public String toString(){
        return text;
    }
    
    @Override
    public void undo() {
        iModel.selectionSet.clear();
        for(int i=0;i<list.size();i++){
            iModel.selectionSet.add(list.get(i));
        }
        movePositionX=prevX-newX;
        movePositionY=prevY-newY;
        model.moveBlobs(list, movePositionX, movePositionY);
        
        
        
        
        
    }
    
}
