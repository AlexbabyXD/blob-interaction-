/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author gutwin
 */
public class Blob implements Groupable{

    double x;
    double y;
    int zOrder;
    double r;

    public Blob(double newX, double newY, double newR, int newZ) {
        x = newX;
        y = newY;
        r = newR;
        zOrder = newZ;
    }

    public boolean contains(double sx, double sy) {
        return r > Math.sqrt(Math.pow(sx - x, 2) + Math.pow(sy - y, 2));
    }

    public void move(double dx, double dy) {
        x += dx;
        y += dy;
    }
    public boolean hasChildren(){
        return false;
    }
    public ArrayList<Groupable> ChildrenList(){
        return null;
    }
    
    public double getLeft(){
        return x-r;
    }
    public double getRight(){
        return x+r;
    }
    public double getTop(){
        return y-r;

    }
    public double getBottom(){
        return y+r;
    }
    public void setZ(int newZ){
        zOrder=newZ;
    }
    public int getZ(){
        return zOrder;
    }

    
    public void draw(GraphicsContext gc,boolean selected){
        if(selected==true){
            gc.setFill(Color.ORANGE);
        }else{
            gc.setFill(Color.LIGHTBLUE);
        }
        gc.fillOval(x - r, y - r, r * 2, r * 2);
        gc.setStroke(Color.BLACK);
        gc.strokeOval(x - r, y - r, r * 2, r * 2);
    }
    public boolean inRectangle(double x1,double y1,double x2,double y2){
        return x-r >= x1 && y-r >= y1 && x+r <= x2 && y+r <= y2;

    }

    @Override
    public Groupable clone() {
        Groupable result = new Blob(this.x,this.y,this.r,this.zOrder);
        return result;
    }

    
    

}
