/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author gutwin
 */
public class BlobModel {
    
    ArrayList<Groupable> entites;           //whatever i have in the model
    ArrayList<ModelListener> subscribers;
    int nextZ;
    Blob thisBlob;

    public BlobModel() {
        entites = new ArrayList<>();
        subscribers = new ArrayList<>();
        nextZ = 0;
    }

    public List<Groupable> getEntites() {
        return entites;
    }

    public Blob addBlob(double x, double y) {
        Blob b = new Blob(x, y, 15, nextZ++);
        entites.add(b);
        notifySubscribers();
        return b;
    }


    public void addSubscriber(ModelListener aSub) {
        subscribers.add(aSub);
    }

    public void notifySubscribers() {
        subscribers.forEach(sub -> sub.modelChanged());
    }

    public boolean contains(double x, double y) {
        return entites.stream()
                .anyMatch(Groupable -> Groupable.contains(x, y));
    }

    public Optional<Groupable> find(double x, double y) {
        return entites.stream()
                .filter(Groupable -> Groupable.contains(x, y))
                .reduce((b1, b2) -> b2);
//        .findFirst();
    }
    
    public List<Groupable> findRubber(double x1, double y1, double x2, double y2) {
        return entites.stream()
                .filter(Groupable -> Groupable.inRectangle(x1, y1, x2, y2))
                .collect(Collectors.toList());
    }

    public void moveBlobs(List<Groupable> blobs, double dx, double dy) {
        blobs.forEach(Groupable -> Groupable.move(dx, dy));
        notifySubscribers();
    }

    public void raiseBlob(Groupable b) {
        if (nextZ == Integer.MAX_VALUE) {
            nextZ = 0;
            entites.forEach(group -> group.setZ(nextZ++));
        } else {
            b.setZ(nextZ++);
        }
        entites.sort((b1, b2) -> b1.getZ() - b2.getZ());
        notifySubscribersMove(0,0);
    }
    
    private void notifySubscribersMove(double dx, double dy) {
        subscribers.forEach(sub -> sub.modelChanged(dx,dy));
    }
    public void deleteBlob(Blob x){
        for(int i=0;i<this.entites.size();i++){
            if(x.equals(entites.get(i))){
                this.entites.remove(i);
            }
        }
       
    }
}
