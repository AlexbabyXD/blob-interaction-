/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;

/**
 *
 * @author alex
 */
public class CreateBlobCommand implements BlobCommand{
    
    Blob b;
    String text;
    BlobModel model;
    InteractionModel iModel;

    
    
    public CreateBlobCommand(Blob k,double dx,double dy,int zorder,BlobModel x,InteractionModel imodel){
        b=k;
        text = "create Blob : "+dx+", " + dy;  
        model=x;
        iModel=imodel;
    }
    @Override
    public void redo() {
        
        model.entites.add(b);
        iModel.selectionSet.clear();
        iModel.selectionSet.add(b);
    }
    @Override
    public String toString(){
        return text;
    }
    

    @Override
    public void undo() {
        this.model.deleteBlob(b);
    }    
    
    
    
}
