/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author gutwin
 */
public class BlobViewController {

    BlobModel model;
    InteractionModel iModel;
    final KeyCombination keyComb1=new KeyCodeCombination(KeyCode.C,KeyCombination.CONTROL_ANY);
    BlobClipboard clipBoard;
    BlobDemo mainDemo;
    double prevXforMove;
    double prevYforMove;


    Stack<BlobCommand> redoStack = new Stack<BlobCommand>();
    Stack<BlobCommand> undoStack = new Stack<BlobCommand>();
    private enum UIState {
        STATE_READY,
        STATE_CONTROL_READY,
        STATE_DRAGGING,
        STATE_CONTROL_DRAGGING,
        STATE_BACKGROUND,
        STATE_CONTROL_BACKGROUND,
        STATE_RUBBERBAND,
        STATE_CONTROL_RUBBERBAND
    }
    UIState state;

    double prevX, prevY, dX, dY;

    public BlobViewController() {
        state = UIState.STATE_READY;
    }

    public void setModel(BlobModel aModel) {
        model = aModel;
    }

    public void setInteractionModel(InteractionModel anIModel) {
        iModel = anIModel;
    }
    public void setClipBoard(BlobClipboard x){
        clipBoard=x;
    }

    
    public void handleMousePressed(MouseEvent event) {
        Optional<Groupable> maybeBlob = model.find(event.getX(), event.getY());
        switch (state) {
            case STATE_READY:
                // on blob or background?
                if (maybeBlob.isPresent()) {
                    // hit blob
                    prevXforMove=event.getX();
                    prevYforMove=event.getY();
                    Groupable b = maybeBlob.get();
                    
                    model.raiseBlob(b);
                    // selected or not?
                    if (iModel.isSelected(b)) {
                        // selected, so just start dragging
                        prevX = event.getX();
                        prevY = event.getY();
                        state = UIState.STATE_DRAGGING;
                    } else {
                        // not selected, so select (only) this blob
                        // and then start dragging
                        iModel.clearSelection();
                        iModel.addSubtractSelection(b);
                        prevX = event.getX();
                        prevY = event.getY();
                        state = UIState.STATE_DRAGGING;
                    }
                } else {
                    // no hit (click is on the background)
                    // clear the selection, and prepare for a rubberband action
                    iModel.clearSelection();
                    state = UIState.STATE_BACKGROUND;
                }
                break;
            case STATE_CONTROL_READY:
                // on blob?
                if (maybeBlob.isPresent()) {
                    // hit blob
                    Groupable b = maybeBlob.get();
                    
                    model.raiseBlob(b);

                    // add/subtract each clicked blob
                    iModel.addSubtractSelection(b);

                    // if we are now on a selected blob,
                    // start dragging
                    if (iModel.isSelected(b)) {
                        // selected, so start dragging
                        prevX = event.getX();
                        prevY = event.getY();
                        state = UIState.STATE_CONTROL_DRAGGING;
                    }
                } else {
                    // no hit (click is on the background)
                    // prepare for a rubberband action
                    state = UIState.STATE_CONTROL_BACKGROUND;
                }
                break;
        }
    }

    
    public void handleMouseDragged(MouseEvent event) {
        switch (state) {
            case STATE_DRAGGING:
                dX = event.getX() - prevX;
                dY = event.getY() - prevY;

                model.moveBlobs(iModel.selectionSet, dX, dY);
                prevX = event.getX();
                prevY = event.getY();
                break;
            case STATE_CONTROL_DRAGGING:
                dX = event.getX() - prevX;
                dY = event.getY() - prevY;
        
                model.moveBlobs(iModel.selectionSet, dX, dY);
                prevX = event.getX();
                prevY = event.getY();
                break;
            case STATE_BACKGROUND:
                // we are starting a rubberband selection
                state = UIState.STATE_RUBBERBAND;
                iModel.setRubberbandStart(event.getX(), event.getY());
                break;
            case STATE_RUBBERBAND:
                // we are continuing a rubberband selection
                iModel.setRubberbandEnd(event.getX(), event.getY());
                break;
            case STATE_CONTROL_BACKGROUND:
                // we are starting a rubberband selection
                state = UIState.STATE_CONTROL_RUBBERBAND;
                iModel.setRubberbandStart(event.getX(), event.getY());
                break;
            case STATE_CONTROL_RUBBERBAND:
                // we are continuing a rubberband selection
                iModel.setRubberbandEnd(event.getX(), event.getY());
                break;
        }
    }

    public void handleMouseReleased(MouseEvent event) {
        List<Groupable> rubberSet;
        switch (state) {
            case STATE_DRAGGING:
              
                MoveCommand MoveCMD = new MoveCommand(iModel.selectionSet,event.getX(),event.getY(),model,iModel);
                MoveCMD.setStartPoint(this.prevXforMove, this.prevYforMove);
                this.undoStack.push(MoveCMD);
                this.mainDemo.updateUndoStack(undoStack);
                
                state = UIState.STATE_READY;
                break;
            case STATE_CONTROL_DRAGGING:
                state = UIState.STATE_CONTROL_READY;
                break;
            case STATE_BACKGROUND:
                // we are just clicking on the background
                // so clear the selection
                iModel.clearSelection();
                state = UIState.STATE_READY;
                break;
            case STATE_RUBBERBAND:
                // we are finishing a rubberband selection
                // so check whether we selected anything
                rubberSet = model.findRubber(iModel.rubber.left, iModel.rubber.top,
                        iModel.rubber.left + iModel.rubber.width, iModel.rubber.top + iModel.rubber.height);
                iModel.addSubtractSelection(rubberSet);
                iModel.deleteRubber();
                state = UIState.STATE_READY;
                break;
            case STATE_CONTROL_RUBBERBAND:
                // we are finishing a rubberband selection
                // so check whether we selected anything
                rubberSet = model.findRubber(iModel.rubber.left, iModel.rubber.top,
                        iModel.rubber.left + iModel.rubber.width, iModel.rubber.top + iModel.rubber.height);
                iModel.addSubtractSelection(rubberSet);
                iModel.deleteRubber();
                state = UIState.STATE_CONTROL_READY;
                break;
        }
    }
    
   

    public void handleKeyPressed(KeyEvent event) {       
        if (event.getCode() == KeyCode.CONTROL) {
            switch (state) {
                case STATE_READY:                    
                    state = UIState.STATE_CONTROL_READY;
                    iModel.setControl(true);
                    break;
                case STATE_DRAGGING:
                    state = UIState.STATE_CONTROL_DRAGGING;
                    iModel.setControl(true);
                    break;
                case STATE_RUBBERBAND:
                    state = UIState.STATE_CONTROL_RUBBERBAND;
                    iModel.setControl(true);
                    break;
            }
        }else if (event.getCode() == KeyCode.A) {
            System.out.println("A");
            Blob x=model.addBlob(Math.random() * 700, Math.random() * 600);
            CreateBlobCommand CreateCMD=new CreateBlobCommand(x,(int)x.x,(int)x.y,model.nextZ++,this.model,iModel);
            this.undoStack.push(CreateCMD);
            this.mainDemo.updateUndoStack(undoStack);
            
        }else if(event.getCode() == KeyCode.U){
            if(iModel.selectionSet!=null){
                for(int k=0;k<model.entites.size();k++){
                    for(int g=0;g<iModel.selectionSet.size();g++){
                        if(model.entites.get(k).equals(iModel.selectionSet.get(g))){
                            model.entites.remove(model.entites.get(k));
                        }
                    }   
                }
            ArrayList<Groupable> groupList = new ArrayList<Groupable>();  
            if(iModel.selectionSet.size()==1){
                for(int f=0;f<iModel.selectionSet.get(0).ChildrenList().size();f++){
                    groupList.add(iModel.selectionSet.get(0).ChildrenList().get(f));
            }
                for(int d=0;d<groupList.size();d++){
                    model.entites.add(groupList.get(d));
            }
            }
        }
        }
        else if(event.getCode() == KeyCode.G){
            if(iModel.selectionSet!=null){
                for(int k=0;k<model.entites.size();k++){
                    for(int g=0;g<iModel.selectionSet.size();g++){
                        if(model.entites.get(k).equals(iModel.selectionSet.get(g))){
                            model.entites.remove(model.entites.get(k));
                        }
                    }   
                }
            ArrayList<Groupable> groupList = new ArrayList<Groupable>();            //create a list for all the selected element
            for(int i=0;i<iModel.selectionSet.size();i++){
                groupList.add(iModel.selectionSet.get(i));
            }
                Group newGroup = new Group(groupList);                                  //Greate a Group element
                model.entites.add(newGroup);
            }
        }else if(event.isControlDown()&&event.getCode() == KeyCode.X){
            if(iModel.selectionSet!=null){
                for(int k=0;k<model.entites.size();k++){
                    for(int g=0;g<iModel.selectionSet.size();g++){
                        if(model.entites.get(k).equals(iModel.selectionSet.get(g))){
                            model.entites.remove(model.entites.get(k));
                    }
                }
            }
        }
     }
        else if(event.isControlDown()&&event.getCode() == KeyCode.C){
            if(iModel.selectionSet!=null){
                clipBoard.board.clear();
                ArrayList<Groupable> groupList = new ArrayList<Groupable>();            //create a list for all the selected element
                for(int i=0;i<iModel.selectionSet.size();i++){
               
                    groupList.add(iModel.selectionSet.get(i).clone());
                }
                clipBoard.board=groupList;  
                mainDemo.clipboardText.setText("Clipboard : "+ clipBoard.board.size());
            }
        }
        else if(event.isControlDown()&&event.getCode() == KeyCode.V){
            
           ArrayList<Groupable> groupList = new ArrayList<Groupable>();            //create a list for all the selected element
           for(int k=0;k<clipBoard.board.size();k++){
               groupList.add(clipBoard.board.get(k).clone());
           }
           for(int i = 0;i<groupList.size();i++){
               model.entites.add(groupList.get(i));
           }
        }
        
        model.notifySubscribers();
       
    }
    


    
    
    
    

    public void handleKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.CONTROL) {
            //System.out.println("Control up");
            switch (state) {
                case STATE_CONTROL_READY:
                    state = UIState.STATE_READY;
                    iModel.setControl(false);
                    break;
                case STATE_CONTROL_DRAGGING:
                    state = UIState.STATE_DRAGGING;
                    iModel.setControl(false);
                    break;
                case STATE_CONTROL_RUBBERBAND:
                    state = UIState.STATE_RUBBERBAND;
                    iModel.setControl(false);
                    break;
            }
        }
        
    }
    public void setDemo(BlobDemo x){
        this.mainDemo=x;
    }
}
